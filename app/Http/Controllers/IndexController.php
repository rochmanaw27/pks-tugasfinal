<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;
use App\Kategori;

class IndexController extends Controller
{
    public function index(){
        $berita = Berita::all();
        $kategori = Kategori::all();
        return view('page.index', compact('berita', 'kategori'));
    }
}
