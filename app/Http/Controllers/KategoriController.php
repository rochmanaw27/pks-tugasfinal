<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Kategori;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Auth;

class KategoriController extends Controller
{
    public function create(){
        return view('kategori.create');
    }

    public function store(Request $request){
        $validated = $request->validate([
            'kategori' => 'required',
        ]);

        DB::table('m_kategori')->insert([
            'kategori' => $request['kategori'],
        ]);
        Alert::success('Berhasil', 'Tambah Data Berhasil');
        return redirect('/kategori');

    }

    public function index(){
        $kategori = Kategori::all();
        return view('kategori.index', compact('kategori'));
    }

    public function show($id){
        $kategori = Kategori::find($id);
        return view('kategori.show', compact('kategori'));
    }

    public function edit($id){
        $kategori = Kategori::find($id);
        return view('kategori.edit', compact('kategori'));
    }

    public function update($id, Request $request){
        $validated = $request->validate([
            'kategori' => 'required',
        ]);

        $kategori = Kategori::find($id);
        $kategori->kategori = $request->kategori;
        $kategori->update();
        Alert::success('Update', 'Tambah Data Berhasil');
        return redirect('/kategori');
    }

    public function destroy($id){
        DB::table('t_comment')
        ->join('t_berita','t_comment.berita_id' ,'=' ,'t_berita.id')
        ->where('t_berita.kategori_id', $id)
        ->delete();

        DB::table('t_berita')->where('kategori_id', $id)->delete();

        $kategori = Kategori::find($id);
        $kategori->delete();
        Alert::warning('Hapus', 'Hapus Data Berhasil');
        return redirect('/kategori');
    }
}
