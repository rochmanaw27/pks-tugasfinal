<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Komentar;
use App\Berita;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Auth;

class KomentarController extends Controller
{
    public function create($id){
        return view('komentar.create', compact('id'));
    }

    public function store(Request $request){
        // $user_id = Auth::id();
        $berita_id = $request->berita_id;

        $validated = $request->validate([
            'comment_content' => 'required',
        ]);

        Komentar::create([
    		'comment_content' => $request->comment_content,
    		'berita_id' => $berita_id,
            'user_id' => Auth::id()
    	]);

        Alert::success('Berhasil', 'Komentar Berhasil');
        return redirect('/berita');
    }

    public function index(){
        $komentar = Komentar::all();
        return view('komentar.index', compact('komentar'));
    }

    public function show($id){
        $komentar = Komentar::find($id);
        return view('komentar.show', compact('komentar'));
    }

    public function edit($id){
        $komentar = Komentar::find($id);
        return view('komentar.edit', compact('komentar'));
    }

    public function update($id, Request $request){
        $user_id = Auth::id();
        $validated = $request->validate([
            'comment_content' => 'required',
            'berita_id' => 'required',
            'user_id' => 'required',
        ]);

        $komentar = Komentar::find($id);
        $komentar->comment_content = $request->comment_content;
        $komentar->update();
        Alert::success('Update', 'Tambah Data Berhasil');
        return redirect('/komentar');
    }

    public function destroy($id){
        $komentar = Komentar::find($id);
        $komentar->delete();
        Alert::warning('Hapus', 'Hapus Data Berhasil');
        return redirect('/komentar');
    }
}
