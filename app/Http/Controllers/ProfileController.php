<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Profile;
use App\User;
use RealRashid\SweetAlert\Facades\Alert;

class ProfileController extends Controller
{
    public function create(){
        return view('profile.create');
    }

    public function store(Request $request){
        $validated = $request->validate([
            'bio' => 'required',
            'birth_date' => 'required',
            'gender' => 'required',
        ]);

        DB::table('t_profile')->insert([
            'birth_date' => $request['birth_date'],
            'bio' => $request['bio'],
            'gender' => $request['gender'],
        ]);
        Alert::success('Berhasil', 'Tambah Data Berhasil');
        return redirect('/profile');
    }

    public function index(){
        $profile = Profile::findOrFail(Auth::id());
        return view('profile.index', compact('profile'));
    }

    public function show($id){
        $profile = DB::table('t_profile')->where('id', $id)->first();
        return view('profile.show', compact('profile'));
    }

    public function edit($id){
        $profile = DB::table('t_profile')->where('id', $id)->first();
        return view('profile.edit', compact('profile'));
    }

    public function update($id, Request $request){
        $user_id = Auth::id();
        $validated = $request->validate([
            'bio' => 'required',
            'birth_date' => 'required',
            'gender' => 'required',
        ]);

        $profile = Profile::find($id);
        $profile->bio = $request->bio;
        $profile->birth_date = $request->birth_date;
        $profile->gender = $request->gender;
        $profile->user_id = $user_id;
        $profile->update();
        Alert::success('Berhasil', 'Edit Profil Berhasil');
        return redirect('/profile');
    }

    public function destroy($id){
        DB::table('t_profile')->where('id', $id)->delete();
        return redirect('/profile');
    }
}
