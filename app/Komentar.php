<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $table = 't_comment';
    protected $fillable = ['comment_content', 'berita_id','user_id', 'created_at'];

    public function user(){
        return $this->belongsTo('App\User');
    }
    public function berita(){
        return $this->belongsTo('App\Berita');
    }
}
