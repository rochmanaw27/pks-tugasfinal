<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 't_profile';
    protected $fillable = ['bio', 'birth_date', 'gender', 'user_id'];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
