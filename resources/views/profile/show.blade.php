@extends('layout.master')
@section('judul')
    Halaman Detail Berita {{$cast->title}}
@endsection

    @section('content')
    <h3>{{$berita->title}}</h3>
    <p>{{$berita->kategori_id}}</p>
    <p>{{$berita->content}}</p>
    @endsection
