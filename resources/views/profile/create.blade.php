@extends('layout.master')
@section('judul')
    Halaman Tambah profile
@endsection

    @section('content')
    <form action="/profile" method="post">
        @csrf
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">Nama Lengkap</label>
          <input type="text" name="fullname" class="form-control">
        </div>
        @error('fullname')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Email</label>
            <input type="text" name="email" class="form-control">
          </div>
          @error('email')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Photo</label>
            <input type="file" name="photo" class="form-control">
          </div>
          @error('photo')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Tanggal Lahir</label>
            <input type="text" name="birth_date" class="form-control">
          </div>
          @error('birth_date')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Telepon</label>
            <input type="text" name="phone" class="form-control">
          </div>
          @error('phone')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        <div class="mb-3">
          <label for="exampleInputPassword1" class="form-label">Biodata</label>
          <textarea name="bio" class="form-control" cols="30" rows="10"></textarea>
        </div>
        @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    @endsection
