@extends('layout.master')
@section('judul')
    Selamat Datang
@endsection

@section('content')

<div class="card card-primary card-outline">
  <!-- Judul Tab -->
  <div class="card-header p-0 pt-1">
    <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
    <li class="nav-item">
          <a class="nav-link active" id="custom-tabs-one-welcome-tab" data-toggle="pill" href="#custom-tabs-one-welcome" role="tab" aria-controls="custom-tabs-one-welcome" aria-selected="true"><b>Selamat Datang</b></a>
      </li>
      @forelse($kategori as $key => $item)
      <li class="nav-item">
          <a class="nav-link" id="custom-tabs-one-{{$item->id}}-tab" data-toggle="pill" href="#custom-tabs-one-{{$item->id}}" role="tab" aria-controls="custom-tabs-one-{$item->id}}" aria-selected="true"><b>{{$item->kategori}}</b></a>
      </li>
     @empty
      @endforelse
    </ul>
  </div>
  <!--  End Judul Tab -->

  <!-- Tab Content -->
  <div class="card-body">
    <div class="tab-content" id="custom-tabs-one-tabContent">
      <!-- Active -->
      <div class="tab-pane fade show active" id="custom-tabs-one-welcome" role="tabpanel" aria-labelledby="custom-tabs-one-welcome-tab">
        <h1>Selamat Datang</h1>
        <h3>Portal<b>Berita</b></h3>
        <p>Merupakan Platform Penyedia Berita dimana Pengguna dapat mengunggah sendiri berita yang ditemui dilapangan.
        </p>
      </div>
      <!-- End Active -->

      @forelse($berita as $key => $news)
      <div class="tab-pane fade" id="custom-tabs-one-{{$news->kategori->id}}" role="tabpanel" aria-labelledby="custom-tabs-one-{{$news->kategori->id}}-tab">
        <div class="col-md-6">
            <!-- Box Comment -->
            <div class="card card-widget">
              <div class="card-header">
                <div class="user-block">
                  <h1>
                    <b>{{$news->title}}</b>
                  </h1>
                  <span>Dibuat pada - {{$news->created_at}}</span>
                </div>
                <!-- /.user-block -->
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <img class="img-fluid" src="{{ asset('data_file/' . $news->photo) }}" alt="Photo">
                <br>
                <p>{{$news->content}}</p>
                <!-- <p>{{$item->content}}</p> -->
                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-comments"></i> Comments</button>
                <button type="button" class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</button>
                <span class="float-right text-muted">127 likes - 3 comments</span>
              </div>
              <!-- /.card-body -->
              <div class="card-footer card-comments">
                <div class="card-comment">
                  <!-- User image -->
                  <div class="comment-text">
                    <span class="username">
                      Maria Gonzales
                      <span class="text-muted float-right">8:03 PM Today</span>
                    </span><!-- /.username -->
                    <p>It is a long established fact that a reader will be distracted
                    by the readable content of a page when looking at its layout.</p>
                    <button type="button" class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</button>
                <span class="float-right text-muted">127 likes</span>
                  </div>
                  <!-- /.comment-text -->
                </div>
              </div>
              <!-- /.card-footer -->
              <div class="card-footer">
                <form action="#" method="post">
                  <div class="img-push">
                    <input type="text" class="form-control form-control-sm" placeholder="Press enter to post comment">
                  </div>
                </form>
              </div>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>
      
        </div>
      </div>
      @empty
      @endforelse
    </div>
  <!-- End Tab Content -->
</div>

@endsection

