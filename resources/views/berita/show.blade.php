@extends('layout.master')
@section('judul')
    Halaman Detail Berita
@endsection

    @section('content')

    <div class="col-md-6">
            <!-- Box Comment -->
            <div class="card card-widget">
              <div class="card-header">
                <div class="user-block">
                  <h1>
                    <b>{{$berita->title}}</b>
                  </h1>
                  <span><b>Dibuat pada -</b> {{$berita->created_at}}</sapan> <br>
                  <span><b>Kategori Berita -</b> {{$berita->kategori->kategori}}</span>

                  <form action="/berita/{{$berita->id}}" method="post">
                      <a href="/berita/{{$berita->id}}/edit" class="btn btn-default btn-sm"><i class="fas fa-edit"></i> Edit Berita</a>
                      @csrf
                      @method('delete')
                      <input type="submit" class="btn btn-default btn-sm" value="Hapus Berita">
                    </form>

                </div>
                <!-- /.user-block -->
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <img class="img-fluid" src="{{ asset('data_file/' . $berita->photo) }}" alt="Photo">
                <br>
                <p>{{strip_tags($berita->content)}}</p>
                <!-- <a href="/berita/{berita_id}" class="btn btn-default btn-sm"><i class="fas fa-comments"></i> Comments</a> -->


              </div>
              <!-- /.card-body -->

              @foreach($berita->komentar as $komen)
              <div class="card-footer card-comments">
                <div class="card-comment">
                  <!-- User image -->
                  <div class="comment-text">
                    <span class="username">
                      {{$komen->user->name}}
                      <span class="text-muted float-right">{{$komen->created_at}}</span>
                    </span><!-- /.username -->
                    <p>{{$komen->comment_content}}</p>
                    <!-- <button type="button" class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</button>
                    <span class="float-right text-muted">{{$berita->id}} likes</span> -->
                  </div>
                  <!-- /.comment-text -->
                </div>
              </div>
              @endforeach
              <!-- /.card-footer -->
              <div class="card-footer">
                <form action="/komentar" method="post">
                    @csrf
                    <div class="img-push">
                        <input type="hidden" name="berita_id" value="{{$berita->id}}">
                        <input type="text" class="form-control form-control-sm" name="comment_content" placeholder="Masukan Komentarmu disini">
                    </div>
                </form>
                <p style="color:red;text-align:right;>">* Tekan enter untuk submit</p>
              </div>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>

    @endsection
