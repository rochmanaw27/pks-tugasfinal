@extends('layout.master')
@section('judul')
    Halaman Tambah Komentar
@endsection
@push('script')
<script src="https://cdn.tiny.cloud/1/53a1xndfmu73tldyhd86jij8c39zsbmsdyvvpypnxatq25tb/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table paste code help wordcount'
    ],
        toolbar: 'undo redo | formatselect | ' +
    'bold italic backcolor | alignleft aligncenter ' +
    'alignright alignjustify | bullist numlist outdent indent | ' +
    'removeformat | help',
        toolbar_mode: 'floating',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
        });
  </script>
@endpush
    @section('content')
    <form action="/komentar" method="post">
        @csrf
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">Komentar</label>
          <textarea name="comment_content" class="form-control" cols="30" rows="10"></textarea>
          <input type="hidden" name="id" value="{{$id}}" class="form-control">
        </div>
        @error('comment_content')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    @endsection
