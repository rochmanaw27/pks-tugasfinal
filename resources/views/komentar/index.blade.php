@extends('layout.master')
@section('judul')
    Halaman List Komentar
@endsection

    @section('content')
    <table class="table">
    <thead class="table-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Title</th>
        <th scope="col">Kategori</th>
        <th scope="col">Photo</th>
        <th scope="col">Komentar</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse($komentar as $key =>$item)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$item->berita->title}}</td>
            <td>{{$item->berita->kategori->kategori}}</td>
            <td>
                <img src="{{ asset('data_file/' . $item->berita->photo) }}" class="css-class" style="width:100px;height:75px;">
            </td>
            <td>{{$item->comment_content}}</td>
            <td>
                <form action="/komentar/{{$item->id}}" method="post">
                    {{-- <a class="btn btn-info btn-sm" href="/komentar/{{$item->id}}">Detail</a>
                    <a class="btn btn-warning btn-sm" href="/komentar/{{$item->id}}/edit">Edit</a> --}}
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
            </td>
        </tr>
      @empty
        <tr>
            <td>Data Masih Kosong</td>
        </tr>
      @endforelse
    </tbody>
  </table>
  @endsection
